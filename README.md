##Willie modules for the FineLine bot

These python files are a collection of modules written for FineLine, an implementation of the [Willie](http://willie.dftba.net/) IRC bot.

Currently written for Willie 4.1.

#Requirements  
beautifulsoup (modules: prbooru.py)  
feedparser (modules: slow_room.py)  
praw (modules: reddit.py, rmlpds_checker.py)  
python-backports.ssl_match_hostname (core for python 2)  
pytz (modules: seen_.py)  